import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;

public class Window extends JFrame implements MouseListener, KeyListener {
	
	ArrayList<Point> points;
	
	public Window() {
		
		super("Barnsleys_Fern");
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setLocationRelativeTo(null);
		super.setSize(1280, 750);
		super.setLocationByPlatform(true);
		super.setVisible(true);
		super.addMouseListener(this);
		super.addKeyListener(this);
		points = new ArrayList<>();

		
	}
	
	@Override
	public void paint(Graphics g) {
		g.setColor(Color.GREEN);
		for(Point p : points) {
			g.fillOval(p.getX(), p.getY(), 3, 3);
		}
		
		
	}
	

	@Override
	public void mousePressed(MouseEvent event) {
		
	}
	
	@Override
	public void keyPressed(KeyEvent event) {

		
	}
	
	public void playGame() {
		
		Random r = new Random();
		final int NUM_TURNS = 10000;
		double x = 0;
		double y = 0;	
		double newX = x, newY = y;
		
		for(int i = 0; i < NUM_TURNS; i++) {
			int action = r.nextInt(100);
			
			if(action < 1) {
				action = 0;
			} else if(action < 86) {
				action = 1;
			} else if(action < 93) {
				action = 2;
			} else {
				action = 3;
			}


			
			
			switch(action) {
			case 0:
				newX = 0;
				newY = y * 0.16;
				break;
			case 1:
				newX = (0.85 * x) + (0.04 * y);
				newY = (-0.04 * x) + (0.85 * y) + 1.60;
				break;
			case 2:
				newX = (0.20 * x) + (-0.26 * y);
				newY = (0.23 * x) + (0.22 * y) + 1.60;
				break;
			case 3:
				newX = (-0.15 * x) + (0.28 * y);
				newY = (0.26 * x) + (0.24 * y) + 0.44;
				break;
				
			}
			
			x = newX;
			y = newY;
			points.add(new Point( (int) (newX*60 + 640), (int) (-newY*60 + 750)));
			repaint();
			
			
			
		}
		
		
		
		
		
	}
	
	
	
	
	//-----------------------GARBAGE-----------------------\\
	

	@Override
	public void mouseClicked(MouseEvent arg0) {}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {}

	@Override
	public void keyReleased(KeyEvent arg0) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}


}
